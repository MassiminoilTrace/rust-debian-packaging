#!/bin/bash

# RustDebianPackaging - A script and docs to help people package
# their own Rust program for Debian-based operating systems.
# 
# Copyright © 2022 Massimo Gismondi
# 
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>. 


# Importing configuration
source "./config.sh"


mkdir -p winbuild
# Create NSIS file
cat > "winbuild/installer.nsi"<< EOF
; The name of the installer
Name "${PROGRAM_FULL_NAME}"

; The file to write
OutFile "${PKG_NAME}.exe"

; The default installation directory
InstallDir \$PROGRAMFILES64\\${PKG_NAME}

; The text to prompt the user to enter a directory
DirText "This will install ${PROGRAM_FULL_NAME} on your computer. Choose a directory"

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

; Set output path to the installation directory.
SetOutPath \$INSTDIR
File start.bat

SetOutPath \$INSTDIR
File ${BUILT_EXECUTABLE}.exe

SetOutPath \$INSTDIR\lib

File lib\libcairo-2.dll lib\libcairo-gobject-2.dll lib\libcrypto-1_1-x64.dll
File lib\libgdk_pixbuf-2.0-0.dll lib\libgdk-3-0.dll lib\libgio-2.0-0.dll
File lib\libglib-2.0-0.dll lib\libgobject-2.0-0.dll lib\libgtk-3-0.dll
File lib\libpango-1.0-0.dll

SetOutPath \$INSTDIR\res
File res\icon.png

SetOutPath \$INSTDIR
CreateDirectory "\$SMPROGRAMS\\${PKG_NAME}"
CreateShortCut "\$SMPROGRAMS\\${PKG_NAME}\pulsantebello.lnk" "\$INSTDIR\\${BUILT_EXECUTABLE}"

WriteUninstaller \$INSTDIR\uninstaller.exe

SectionEnd ; end the section


Section "Uninstall"

Delete "\$SMPROGRAMS\\${PKG_NAME}\pulsantebello.lnk"
 
Delete \$INSTDIR\*
 
# Delete the uninstaller
Delete \$INSTDIR\uninstaller.exe
 
# Delete the directory
RMDir \$INSTDIR
SectionEndEOF
EOF

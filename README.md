# Introduction
This is a repository with a script and docs to help people package their own Rust program for Debian-based operating systems.

The project started as a tool to package a GTK graphical application I made, make sure to tweak the script for your own goals.

# Setup
Aside from the rust / cargo toolchain, you need to install `dpkg-deb` from your package manager.

# Usage
Clone the repository
`git clone https://gitlab.com/MassiminoilTrace/rust-debian-packaging.git`

Navigate to the folder using  
`cd rust-debian-packaging`

Edit the `config.sh` file in the downloaded folder adding details with your favourite editor.

Then, simply run  
`bash ./package.sh`

## Windows NSIS installer (WIP!)
You can generate a `.nsi` script to use with NSIS by running `bash ./package_windows.sh`.
The generated script will appear in the `winbuild` directory. After that, run NSIS to build the installer executable.

# Copyright
Copyright © 2022 Massimo Gismondi

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.



cross build --target arm-unknown-linux-gnueabihf

docker build --tag=armgtk3 .


docker run -it armgtk3 bash
#!/bin/bash

# RustDebianPackaging - A script and docs to help people package
# their own Rust program for Debian-based operating systems.
# 
# Copyright © 2022 Massimo Gismondi
# 
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>. 

PROGRAM_FULL_NAME="Calcola MB"
PROGRAM_DESCRIPTION="Insert here a description"
PKG_NAME="calcolamb"
BUILT_EXECUTABLE="gtk-test"
VERSION="1.0"
PKG_NUMBER="1"
MAINTAINER_NAME="Mario Rossi"
MAINTAINER_EMAIL="hello@test.something.com"
DEPENDENCIES="libgtk-3-0"

#where to find the source code?
SOURCE_CODE_URL="https://gitlab/SOMETHING"
COPYRIGHT_YEAR="2022"
COPYRIGHT_AUTHOR_NAME="Name of Author"
COPYRIGHT_AUTHOR_EMAIL="hello@test.something.com"
LICENSE="GPL-3+"

# Where to find files to copy?
RUST_PROJECT_FOLDER="../gtk-test"
# Must be a PNG format
ICON_PATH="../gtk-test/res/icon.png"
#!/bin/bash

# RustDebianPackaging - A script and docs to help people package
# their own Rust program for Debian-based operating systems.
# 
# Copyright © 2022 Massimo Gismondi
# 
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>. 


# Importing configuration
source "./config.sh"

# Compile program in release mode
ARCHITECTURE=amd64
(cd "${RUST_PROJECT_FOLDER}";cargo build --release --target x86_64-unknown-linux-gnu)



PKG_FOLDER="${PKG_NAME}_${VERSION}_${PKG_NUMBER}_${ARCHITECTURE}"

#Building folder structure
mkdir -p "${PKG_FOLDER}"
mkdir -p "${PKG_FOLDER}/DEBIAN"
mkdir -p "${PKG_FOLDER}/usr/bin"

mkdir -p "${PKG_FOLDER}/usr/share/applications"

mkdir -p "${PKG_FOLDER}/usr/share/${PKG_NAME}/icons"

# Create control file
cat > "${PKG_FOLDER}/DEBIAN/control"<< EOF
Package: ${PKG_NAME}
Version: ${VERSION}
Architecture: ${ARCHITECTURE}
Maintainer: ${MAINTAINER_NAME} <${MAINTAINER_EMAIL}>
Depends: ${DEPENDENCIES}
Priority: Optional
Description: ${PROGRAM_DESCRIPTION}
EOF


# Create Copyright file
cat > "${PKG_FOLDER}/DEBIAN/copyright"<< EOF
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ${PROGRAM_FULL_NAME}
Source: ${SOURCE_CODE_URL}

Files: *
Copyright: ${COPYRIGHT_YEAR} ${COPYRIGHT_AUTHOR_NAME} <${COPYRIGHT_AUTHOR_EMAIL}>
License: ${LICENSE}
EOF



# Copy files to the correct place, changing name of executable to PKG_NAME
cp "${RUST_PROJECT_FOLDER}/target/release/${BUILT_EXECUTABLE}" "${PKG_FOLDER}/usr/bin/${PKG_NAME}"
cp "${ICON_PATH}" "${PKG_FOLDER}/usr/share/${PKG_NAME}/icons/icon.png" 


# Create menu item shortcut
cat > "${PKG_FOLDER}/usr/share/applications/${PKG_NAME}.desktop"<< EOF
[Desktop Entry]
Name=${PROGRAM_FULL_NAME}
Comment=Restituisce la grandezza di un file
GenericName=Calcolatore MB
Terminal=false
X-MultipleArgs=false
Type=Application
StartupNotify=true
Version=${VERSION}
Exec=${PKG_NAME}
Icon=/usr/share/${PKG_NAME}/icons/icon.png
EOF



# Generate .deb
dpkg-deb --build --root-owner-group "${PKG_FOLDER}"

# Delete temporary folder
rm -rf ${PKG_FOLDER}